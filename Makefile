PREFIX?=/usr/local
LIBRARY_PATH?=lib
INSTALL_LIBRARY_PATH= $(DESTDIR)$(PREFIX)/$(LIBRARY_PATH)
build:
	$(CC) numlock.c -c -o numlock.o `pkgconf x11 --cflags`
	$(CC) numlock.o -shared -o libnumlock.so `pkgconf x11 --libs`
install:
	mkdir -p $(INSTALL_LIBRARY_PATH)
	install libnumlock.so $(INSTALL_LIBRARY_PATH)
clean:
	rm -f *.so *.o
uninstall:
	rm -f $(INSTALL_LIBRARY_PATH)/libnumlock.so

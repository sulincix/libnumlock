#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

int is_numlock_enabled(){
   Display *dpy = XOpenDisplay(getenv("DISPLAY"));
   XKeyboardState x;
   XGetKeyboardControl(dpy, &x);
   XCloseDisplay(dpy);
   if(x.led_mask & 2){
      return 1;
   }
   return 0;
}

int is_capslock_enabled(){
   Display *dpy = XOpenDisplay(getenv("DISPLAY"));
   XKeyboardState x;
   XGetKeyboardControl(dpy, &x);
   XCloseDisplay(dpy);
   if(x.led_mask & 1){
      return 1;
   }
   return 0;
}

